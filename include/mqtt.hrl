-define(APPNAME, mqtt).
-define(MQTT_BROKER, "10.0.0.44").
-define(MQTT_CLIENT_NAME, <<"mqtt-client">>).
-define(MQTT_DEFAULT_TOPIC, <<"default">>).
-define(MQTT_DEFAULT_MESSAGE, <<"testing broker with default message">>).
-define(MQTT_RECONNECT_ATTEMPT_INTERVAL, 5000).

-define(MQTT_TEST_TOPIC,<<"test-topic">>).
-define(MQTT_TEST_MESSAGE,<<"test-message">>).
-define(MQTT_TEST_QOS, 1).
-define(MQTT_TEST_WAIT_INTERVAL,5000).