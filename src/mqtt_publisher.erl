-module (mqtt_publisher).
-behaviour (gen_server).
-include ("mqtt.hrl").
-include ("debug.hrl").


-export([start/0,
  stop/0]).

-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([state/0,
  publish/0,
  publish/1,
  publish/2]).

-record(state, {mqttc=undefined, topic=undefined}).

start()->
  gen_server:start_link({local,?MODULE},?MODULE,[],[]).

stop()->
  gen_server:call(?MODULE,stop).

state()->
  gen_server:call(?MODULE,state).

publish()->
  gen_server:call(?MODULE,{publish,?MQTT_DEFAULT_MESSAGE}).

publish(Message)->
  gen_server:call(?MODULE,{publish,Message}).

publish(Message,Topic)->
  gen_server:call(?MODULE,{publish,Message,Topic}).

% Callbacks

init([])->
  process_flag(trap_exit,true),
  Host=get_config(mqtt_broker,'cerberus.local'),
  ClientName=get_config(mqtt_client_name,'noname'),
  Topic=get_config(mqtt_publish_topic,<<"default">>),
  {ok, C} = emqttc:start_link([{host, Host},
    {client_id, ClientName},
    {reconnect, 10},
    {logger, debug}]),
  State=#state{mqttc=C, topic=Topic},
  ?LOG_INFO({started,[{client_id,ClientName},{host,Host}]}),
  {ok,State}.

handle_call(stop,_from,State)->
  {stop,normal,ok,State};

handle_call(state,_from,State)->
  {reply,State,State};

handle_call({publish,Message,Topic},_from,State=#state{mqttc=C})->
  R=i_publish(C,Topic,Message),
  {reply,R,State};

handle_call({publish,Message},_from,State=#state{mqttc=C, topic=Topic})->
  R=i_publish(C,Topic,Message),
  {reply,R,State};

handle_call(Msg,_From,State)->
  ?LOG_WARNING({unhandled_call,{msg,Msg},{state,State}}),
  {reply,ignored,State}.

handle_cast(Msg,State)->
  ?LOG_WARNING({unhandled_cast,{msg,Msg},{state,State}}),
  {noreply,State}.

handle_info({mqttc,C,connected}, State=#state{mqttc=C}) ->
  ?LOG_INFO({client_connected,C}),
  {noreply, State};

handle_info({mqttc,C,disconnected}, State=#state{mqttc=C}) ->
  ?LOG_INFO({client_disconnected,C}),
  {noreply, State};

handle_info({'EXIT',C,Reason},State=#state{mqttc=C})->
  ?LOG_ERROR({broker_exited,{reason,Reason},{state,State}}),
  {noreply,State#state{mqttc = undefined}};

handle_info(Msg,State)->
  ?LOG_WARNING({unhandled_info,{msg,Msg},{state,State}}),
  {noreply,State}.

code_change(_OldVsn,State,_Extra) ->
  {ok,State}.

terminate(Reason,#state{mqttc=C})->
  ?LOG_WARNING({terminating,Reason}),
  emqttc:disconnect(C),
  ok.

%% private functions

get_config(Key,Default)->
  case application:get_env(?APPNAME,Key) of
    undefined->
      ?LOG_WARNING({using_default,{key,Key},{value,Default}}),
      Default;
    {ok, Value}->
      ?LOG_INFO({app_config,{key,Key},{value,Value}}),
      Value
  end.

i_publish(Broker,Topic,Message)->
  Json=jsx:encode(Message),
  R=emqttc:publish(Broker,Topic,Json),
  R.